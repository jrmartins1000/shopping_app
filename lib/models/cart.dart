import 'dart:math';

import 'package:flutter/material.dart';
import 'package:shop/models/cart_item.dart';
import 'package:shop/models/product.dart';

class Cart with ChangeNotifier {
  Map<String, CartItem> _items = {};

  Map<String, CartItem> get items {
    // TODO : CLONE DO MAP PRINCIPAL ACIMA
    return {..._items};
  }

  //TODO : METODO QUANTOS ITENS TEM NA LSITA
  int get itemsCount {
    return _items.length;
  }

  //TODO : TOTAL DE ITEM
  double get totalAmount {
    double total = 0.0;

    _items.forEach((key, carItem) {
      total += carItem.price * carItem.quantity;
    });
    return total;
  }

  //TODO : METODO ADICIONAR ITEMS
  void addItem(Product product) {
    if (_items.containsKey(product.id)) {
      // TODO : SOMANDO UM NOVO ITEM QUE JÁ ESTÁ NO CARRINHO
      _items.update(
        product.id,
        (existingItem) => CartItem(
            id: existingItem.id,
            productId: existingItem.productId,
            name: existingItem.name,
            quantity: existingItem.quantity + 1,
            price: existingItem.price),
      );
    } else {
      _items.putIfAbsent(
        //TODO : ADICIONANDO UM NOVO ITEM
        product.id,
        () => CartItem(
            id: Random().nextDouble().toString(),
            productId: product.id,
            name: product.name,
            quantity: 1,
            price: product.price),
      );
    }
    notifyListeners();
  }

  //TODO : REMOVE ITEM DA LISTA
  void removeItem(String productId) {
    _items.remove(productId);
    notifyListeners();
  }

  //TODO : REMOVE UM ÚNICO ELEMENTO
  void removeSingleItem(String productId){
    if(!_items.containsKey(productId)){
      return;
    }

    if(_items[productId]?.quantity == 1){
      _items.remove(productId);
    }else{
      _items.update(
        productId,
            (existingItem) => CartItem(
            id: existingItem.id,
            productId: existingItem.productId,
            name: existingItem.name,
            quantity: existingItem.quantity - 1,
            price: existingItem.price),
      );

    }
    notifyListeners();

  }

  //TODO : LIMPA A LISTA
  void clear() {
    _items = {};
    notifyListeners();
  }
}
