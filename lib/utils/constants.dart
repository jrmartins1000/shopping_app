class Constants{
  static const BASE_URL = "https://shop-loja-default-rtdb.firebaseio.com/";
  static const PRODUCT_FIREBASE = "products.json";
  static const PRODUCTS = "/products/";
  static const ORDERS = "orders";

}